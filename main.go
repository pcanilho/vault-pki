package main

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/pcanilho/vault-pki/cmd"
)

func main() {
	if err := cmd.Execute(); err != nil {
		logrus.Fatalln(err)
	}
}
