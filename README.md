# `vault-pki` made in `GoLang`

The aim of this project is to offer an extension to the already awesome [Hashicorp Vault PKI](https://www.vaultproject.io/)
system.

## `Features`

* Authenticate with the `vault` backend system without needing more than a `vault login` a priori.
* Search for / revoke issued certificates based on their `common-name`, `SAN`s or `serial`.
* Offer `visualisation` & `searching` capabilities based on common-name `CN` or `serial`.
* Sorting & Filtering of `status`, `valid_from`, `valid_to` and more!
* Multiple output formats to ease integrations with other applications in `json`, `yaml` or `table`.

Note: By default only `valid` certificates are outputted. If you would like to also see `invalid` or `revoked` ones, please supply the `-a` or `--all` flag. 

## Examples

### `List` all available certificates for the specified PKI backend and sort by `valid_to`

```sh
vault-pki list --sort valid_to
```

### `Search` allows multiple arguments to be supplied. Example using the `common-name`, `SAN` and/or `serial`

```sh
vault-pki search <commonName> <altName> <serial>
```

## Usage

```sh
Usage:
vault-pki [command]

Available Commands:
browser
completion  generate the autocompletion script for the specified shell
help        Help about any command
list
revoke
search
status
tidy

Flags:
--ASC             When sorting is used, if this flag is supplied the output will be sorted in Ascending order
-a, --all             Show all matches for the provided CN.
-d, --debug           Display debug information when provided.
-e, --expanded        If set to True, the output will include additional information.
-f, --format string   The output format to be used by the application. Supported: [json, yaml, table] (default "yaml")
-h, --help            help for vault-pki
-p, --pki string      The mount-point that should be used to query the Vault PKI engine (default "rsa-pki")
-s, --sort strings    Sorting options. Available options: [status, serial, subject, valid_from, valid_to]. Several options may be provided and will be used to Sort the output incrementally. Order defaults to DESC. (default [subject])
-t, --top int         If a value greater than 0 is specified, only the specified amount of results will be shown after sorting
-v, --version         version for vault-pki

Use "vault-pki [command] --help" for more information about a command.
```
