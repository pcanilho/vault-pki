package cmd

import (
	"fmt"
	"os/exec"
	"runtime"
)

func openLinkInBrowser(link string) error {
	switch runtime.GOOS {
	case "linux":
		return exec.Command("xdg-open", link).Start()
	case "windows":
		return exec.Command("rundll32", "url.dll,FileProtocolHandler", link).Start()
	case "darwin":
		return exec.Command("open", link).Start()
	default:
		return fmt.Errorf("unsupported platform")
	}
}
