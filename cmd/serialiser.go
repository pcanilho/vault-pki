package cmd

import (
	"encoding/json"
	"fmt"
	"github.com/olekukonko/tablewriter"
	"gitlab.com/pcanilho/vault-pki/internal"
	"gopkg.in/yaml.v3"
	"os"
	"sort"
)

type Serialiser interface {
	Serialise(interface{}) string
}

type jsonSerialiser struct{}

func (j *jsonSerialiser) Serialise(i interface{}) string {
	c, _ := json.MarshalIndent(i, "", " ")
	return string(c)
}

func NewJSONSerialiser() *jsonSerialiser {
	return new(jsonSerialiser)
}

type yamlSerialiser struct{}

func NewYAMLSerialiser() *yamlSerialiser {
	return new(yamlSerialiser)
}

func (y *yamlSerialiser) Serialise(i interface{}) string {
	c, _ := yaml.Marshal(i)
	return fmt.Sprintf("---\n%s", string(c))
}

type tableSerialiser struct{}

func NewTableSerialiser() *tableSerialiser {
	return new(tableSerialiser)
}

func (t *tableSerialiser) Serialise(i interface{}) string {
	var global []map[string]interface{}
	concrete := i.([]*internal.Certificate)
	bs, _ := yaml.Marshal(concrete)
	_ = yaml.Unmarshal(bs, &global)
	var headers []string
	for k := range global[0] {
		headers = append(headers, k)
	}
	sort.SliceStable(headers, func(i, j int) bool {
		return headers[i] < headers[j]
	})

	table := tablewriter.NewWriter(os.Stdout)
	table.SetAutoFormatHeaders(true)
	table.SetAlignment(tablewriter.ALIGN_LEFT)
	table.SetHeader(headers)
	//table.SetAutoMergeCells(true)

	for _, e := range global {
		var row []string
		for _, h := range headers {
			row = append(row, fmt.Sprintf("%v", e[h]))
		}
		table.Append(row)
	}
	table.Render()
	return ""
}
