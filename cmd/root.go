package cmd

import (
	"fmt"
	nested "github.com/antonfisher/nested-logrus-formatter"
	vault "github.com/hashicorp/vault/api"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/pcanilho/vault-pki/internal"
	"os"
	"strings"
	"time"
)

/*************************** Command ***************************/
var (
	version string
	name    = "vault-pki"
)

/*** Manager ***/
var certManager *internal.CertificateManager
var serialiser Serialiser

/*** Persistent flags ***/
var pkiMountPoint string
var outputFormat string

/*** Limiters ***/
var topResults int
var showAll bool
var expanded bool

//var onlyExpired bool
//var onlyValid bool

/*** Sorting ***/
var selectedSorting []string
var ascOrderSelected bool

/*** Stats ***/
var debug bool
var startTime time.Time

var rootCommand = &cobra.Command{
	Use:     name,
	Version: fmt.Sprintf("(%s)", version),
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		// Logger
		if debug {
			logrus.SetLevel(logrus.TraceLevel)
		}

		// Serialiser
		switch strings.TrimSpace(strings.ToLower(outputFormat)) {
		case "yaml":
			serialiser = NewYAMLSerialiser()
		case "json":
			serialiser = NewJSONSerialiser()
		case "table":
			serialiser = NewTableSerialiser()
		default:
			logrus.Fatalln("Unsupported output format supplied")
		}

		// Login
		client, err := vault.NewClient(vault.DefaultConfig())
		if err != nil {
			logrus.Fatalln(errors.Wrap(err, "unable to create the vault client"))
		}

		// Get token file if environment is not set
		if len(strings.TrimSpace(os.Getenv("VAULT_TOKEN"))) == 0 {
			logrus.Debugf("The variable [VAULT_TOKEN] was not found. Attempting to default to credentials file...")
			if home, err := os.UserHomeDir(); err == nil {
				if tknBytes, err := os.ReadFile(fmt.Sprintf("%s/.vault-token", home)); err == nil {
					client.SetToken(string(tknBytes))
				}
			}
		}

		// Lookup PKI
		certManager = new(internal.CertificateManager)
		if err = certManager.Initialise(client, pkiMountPoint); err != nil {
			logrus.Fatalln(err)
		}

		// Assign sorter
		selectedOrder := DESC
		if ascOrderSelected {
			selectedOrder = ASC
		}
		certManager.SortFunc = NewSorter(selectedSorting, selectedOrder).Sort
		startTime = time.Now()
	},
	PersistentPostRun: func(cmd *cobra.Command, args []string) {
		logrus.Tracef("Elapsed time: %v", time.Now().Sub(startTime).String())
	},
}

func Execute() error {
	return rootCommand.Execute()
}

func init() {
	loggerSetup()

	rootCommand.PersistentFlags().BoolVarP(&debug, "debug", "d", false,
		"Display debug information when provided.")
	rootCommand.PersistentFlags().StringVarP(&pkiMountPoint, "pki", "p", "rsa-pki",
		"The mount-point that should be used to query the Vault PKI engine")
	rootCommand.PersistentFlags().StringVarP(&outputFormat, "format", "f", "yaml",
		"The output format to be used by the application. Supported: [json, yaml, table]")

	rootCommand.PersistentFlags().IntVarP(&topResults, "top", "t", 0,
		"If a value greater than 0 is specified, only the specified amount of results will be shown after sorting")
	rootCommand.PersistentFlags().StringSliceVarP(&selectedSorting, "sort", "s", []string{"subject"},
		"Sorting options. Available options: [status, serial, subject, valid_from, valid_to]."+
			" Several options may be provided and will be used to Sort the output incrementally. Order defaults to DESC.")
	rootCommand.PersistentFlags().BoolVarP(&showAll, "all", "a", false,
		"Show all matches for the provided CN.")
	//rootCommand.PersistentFlags().BoolVar(&onlyValid, "valid", false,
	//	"Show only valid certificates.")
	//rootCommand.PersistentFlags().BoolVar(&onlyExpired, "expired", true,
	//	"Show only invalid certificates.")
	rootCommand.PersistentFlags().BoolVar(&ascOrderSelected, "ASC", false,
		"When sorting is used, if this flag is supplied the output will be sorted in Ascending order")
	rootCommand.PersistentFlags().BoolVarP(&expanded, "expanded", "e", false,
		"If set to True, the output will include additional information.")

	rootCommand.AddCommand(searchCommand)
	rootCommand.AddCommand(listCommand)
	rootCommand.AddCommand(tidyCommand)
	rootCommand.AddCommand(tidyStatusCommand)
	rootCommand.AddCommand(revokeCommand)
	rootCommand.AddCommand(browserCertificateCommand)
}

func loggerSetup() {
	logrus.SetFormatter(&nested.Formatter{
		HideKeys:        true,
		ShowFullLevel:   true,
		TimestampFormat: time.RFC3339,
	})
	logrus.SetLevel(logrus.ErrorLevel)
}
