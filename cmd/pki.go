package cmd

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/pcanilho/vault-pki/internal"
)

var searchCommand = &cobra.Command{
	Use:     "search",
	Aliases: []string{"get", "read"},
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Print(serialiser.Serialise(certManager.Search(showAll, topResults, args,
			internal.WithExpandedOutput(expanded))))
	},
}

var listCommand = &cobra.Command{
	Use: "list",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Print(serialiser.Serialise(certManager.ListAll(showAll, topResults,
			internal.WithExpandedOutput(expanded))))
	},
}

var revokeCommand = &cobra.Command{
	Use:     "revoke",
	Aliases: []string{"rm", "r"},
	RunE: func(cmd *cobra.Command, args []string) error {
		out, err := certManager.Revoke(args...)
		if err != nil {
			return err
		}
		fmt.Print(serialiser.Serialise(out))
		return nil
	},
}

var certStore, revokedCerts bool
var safetyBuffer string
var tidyCommand = &cobra.Command{
	Use: "tidy",
	RunE: func(cmd *cobra.Command, _ []string) error {
		out, err := certManager.Tidy(safetyBuffer, certStore, revokedCerts)
		if err != nil {
			return err
		}
		fmt.Print(serialiser.Serialise(out))
		return nil
	},
}

var tidyStatusCommand = &cobra.Command{
	Use:     "status",
	Aliases: []string{"tidy-status", "s", "ts"},
	Run: func(cmd *cobra.Command, args []string) {
		out, err := certManager.TidyStatus()
		if err != nil {
			logrus.Fatalln(err)
		}
		fmt.Print(serialiser.Serialise(out))
	},
}

var browserCertificateCommand = &cobra.Command{
	Use:     "browser",
	Aliases: []string{"open", "show"},
	RunE: func(cmd *cobra.Command, args []string) error {
		return openLinkInBrowser(certManager.Search(false, -1, args, internal.WithExpandedOutput(true))[0].Link)
	},
}

func init() {
	tidyCommand.PersistentFlags().BoolVar(&certStore, "cert_store", false,
		"Specifies whether to tidy up the certificate store.")
	tidyCommand.PersistentFlags().BoolVar(&revokedCerts, "revoked_certs", false,
		"Set to true to remove all invalid and expired certificates from storage."+
			" A revoked storage entry is considered invalid if the entry is empty, or the"+
			" value within the entry is empty. If a certificate is removed due to expiry,"+
			" the entry will also be removed from the CRL, and the CRL will be rotated.")
	tidyCommand.PersistentFlags().StringVar(&safetyBuffer, "safety_buffer", "72h",
		"Specifies A duration (given as an integer number of seconds or a string; "+
			"defaults to 72h) used as a safety buffer to ensure certificates are not expunged "+
			"prematurely; as an example, this can keep certificates from being removed from the "+
			"CRL that, due to clock skew, might still be considered valid on other hosts. For a "+
			"certificate to be expunged, the time must be after the expiration time of the "+
			"certificate (according to the local clock) plus the duration of safety_buffer.")
}
