package cmd

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/pcanilho/vault-pki/internal"
	"sort"
	"strings"
	"time"
)

var sortingMap = map[string]func([]*internal.Certificate, sortOrder){
	"status":     sortByStatus,
	"subject":    sortBySubject,
	"serial":     sortBySerial,
	"valid_from": sortByValidFrom,
	"valid_to":   sortByValidUntil,
}

type sorter struct {
	selectedSorting []string
	selectedOrder   sortOrder
}

type sortOrder = int

const (
	ASC sortOrder = iota
	DESC
)

func NewSorter(selectedSorting []string, order sortOrder) *sorter {
	return &sorter{selectedSorting, order}
}

func (s *sorter) Sort(in []*internal.Certificate) {
	for _, sorting := range s.selectedSorting {
		sorting = strings.TrimSpace(strings.ToLower(sorting))
		if fn, supported := sortingMap[sorting]; supported {
			fn(in, s.selectedOrder)
		} else {
			logrus.Fatalln("Sorting option not supported!")
		}
	}
}

func sortByStatus(in []*internal.Certificate, o sortOrder) {
	sort.SliceStable(in, func(i, j int) bool {
		if o == DESC {
			return in[i].Status < in[j].Status
		} else {
			return in[i].Status > in[j].Status
		}
	})
}

func sortBySubject(in []*internal.Certificate, o sortOrder) {
	sort.SliceStable(in, func(i, j int) bool {
		if o == DESC {
			return in[i].Subject < in[j].Subject
		} else {
			return in[i].Subject > in[j].Subject
		}
	})
}

func sortBySerial(in []*internal.Certificate, o sortOrder) {
	sort.SliceStable(in, func(i, j int) bool {
		if o == DESC {
			return in[i].Serial < in[j].Serial
		} else {
			return in[i].Serial > in[j].Serial
		}
	})
}

func sortByValidFrom(in []*internal.Certificate, o sortOrder) {
	sort.SliceStable(in, func(i, j int) bool {
		a, _ := time.Parse(time.RFC3339, in[i].ValidFrom)
		b, _ := time.Parse(time.RFC3339, in[j].ValidFrom)
		if o == DESC {
			return a.After(b)
		} else {
			return a.Before(b)
		}
	})
}

func sortByValidUntil(in []*internal.Certificate, o sortOrder) {
	sort.SliceStable(in, func(i, j int) bool {
		a, _ := time.Parse(time.RFC3339, in[i].ValidTo)
		b, _ := time.Parse(time.RFC3339, in[j].ValidTo)
		if o == DESC {
			return a.After(b)
		} else {
			return a.Before(b)
		}
	})
}
