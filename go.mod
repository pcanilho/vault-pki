module gitlab.com/pcanilho/vault-pki

go 1.16

require (
	github.com/antonfisher/nested-logrus-formatter v1.3.1
	github.com/hashicorp/vault/api v1.3.0
	github.com/olekukonko/tablewriter v0.0.5
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v1.2.1
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
