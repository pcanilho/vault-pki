package internal

import (
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	vault "github.com/hashicorp/vault/api"
	"net"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Certificate struct {
	Subject, Serial string
	IPs             []net.IP `yaml:"ip_san,omitempty" json:"ip_san,omitempty"`
	AltNames        []string `yaml:"alt_names,omitempty" json:"alt_names,omitempty"`
	ValidFrom       string   `yaml:"valid_from" json:"valid_from"`
	ValidTo         string   `yaml:"valid_to" json:"valid_to"`
	Status          string
	Link            string `yaml:",omitempty" json:",omitempty"`
	KeySize         int    `yaml:"key_size" json:"key_size"`
	RevokedAt       string `yaml:"revoked_at,omitempty" json:"revoked_at,omitempty"`
}

func (c *Certificate) containsAltName(altName string) bool {
	if c.AltNames == nil || len(c.AltNames) == 0 {
		return false
	}
	for _, n := range c.AltNames {
		if strings.HasPrefix(n, altName) {
			return true
		}
	}
	return false
}

type ManagerOption = func(*CertificateManager)

func WithExpandedOutput(expanded bool) ManagerOption {
	return func(manager *CertificateManager) {
		manager.expanded = expanded
	}
}

type CertificateStatus = string

const (
	Expired = "EXPIRED"
	Valid   = "VALID"
	Revoked = "REVOKED"
)

func NewCertificate(cm *CertificateManager, c certificateEntry, serial string) *Certificate {
	status := Valid
	if time.Now().After(c.certificate.NotAfter) {
		status = Expired
	}
	var revocationFormatted string
	if c.revocationTime.Year() != 1970 {
		status = Revoked
		revocationFormatted = c.revocationTime.Format(time.RFC3339)
	}
	var link string
	if cm.expanded {
		link = fmt.Sprintf("%s/ui/vault/secrets/%s/show/cert/%s", cm.client.Address(), cm.pkiMountPoint, serial)
	}

	var bitLen int
	switch publicKey := c.certificate.PublicKey.(type) {
	case *rsa.PublicKey:
		bitLen = publicKey.N.BitLen()
	case *ecdsa.PublicKey:
		bitLen = publicKey.Curve.Params().BitSize
	default:
		return nil
	}

	return &Certificate{
		Subject:   c.certificate.Subject.String(),
		Serial:    serial,
		IPs:       c.certificate.IPAddresses,
		AltNames:  c.certificate.DNSNames,
		ValidFrom: c.certificate.NotBefore.Format(time.RFC3339),
		ValidTo:   c.certificate.NotAfter.Format(time.RFC3339),
		Status:    status,
		KeySize:   bitLen,
		Link:      link,
		RevokedAt: revocationFormatted,
	}
}

type CertificateManager struct {
	SortFunc      func([]*Certificate)
	certMap       map[string]certificateEntry
	logical       *vault.Logical
	client        *vault.Client
	pkiMountPoint string
	expanded      bool
}

type certificateEntry struct {
	revocationTime time.Time
	certificate    *x509.Certificate
}

func (cm *CertificateManager) Initialise(client *vault.Client, pkiMountPoint string) error {
	cm.logical = client.Logical()
	cm.client = client
	cm.pkiMountPoint = pkiMountPoint
	certificateList, err := cm.logical.List(fmt.Sprintf("%s/certs", cm.pkiMountPoint))
	if err != nil {
		return err
	}

	var mx sync.RWMutex
	var wg sync.WaitGroup
	cm.certMap = make(map[string]certificateEntry)
	for _, v := range certificateList.Data {
		vC := v.([]interface{})
		wg.Add(len(vC))
		for _, serial := range vC {
			go func(s interface{}) {
				certData, _ := cm.logical.Read(fmt.Sprintf("%s/cert/%v", cm.pkiMountPoint, s))
				derBlock, _ := pem.Decode([]byte(certData.Data["certificate"].(string)))
				revokedTimestamp, _ := certData.Data["revocation_time"]
				cert, _ := x509.ParseCertificate(derBlock.Bytes)
				revokedTimeUnit, _ := strconv.ParseInt(fmt.Sprint(revokedTimestamp), 10, 64)
				mx.Lock()
				cm.certMap[s.(string)] = certificateEntry{
					revocationTime: time.Unix(revokedTimeUnit, 0),
					certificate:    cert,
				}
				mx.Unlock()
				wg.Done()
			}(serial)
		}
		wg.Wait()
	}

	return nil
}

func (cm *CertificateManager) Search(all bool, top int, ids []string, opts ...ManagerOption) (out []*Certificate) {
	cm.applyOpts(opts...)
	var it int
	var tmp []*Certificate
	for serial, entry := range cm.certMap {
		tmp = append(tmp, NewCertificate(cm, entry, serial))
	}
	if cm.SortFunc != nil {
		cm.SortFunc(tmp)
	}

	for _, id := range ids {
		for _, cert := range tmp {
			if top > 0 && it >= top {
				return
			}
			if strings.Contains(cert.Subject, id) || strings.Contains(cert.Serial, id) || cert.containsAltName(id) {
				if !all && cert.Status != Expired && cert.Status != Revoked {
					it++
					out = append(out, cert)
				}
				if all {
					it++
					out = append(out, cert)
				}
			}
		}
	}
	return
}

func (cm *CertificateManager) Revoke(ids ...string) (map[string]*vault.Secret, error) {
	out := make(map[string]*vault.Secret, len(ids))
	for _, id := range ids {
		certificates := cm.Search(false, 0, ids)
		for _, cert := range certificates {
			response, err := cm.logical.Write(fmt.Sprintf("%s/revoke", cm.pkiMountPoint), map[string]interface{}{
				"serial_number": cert.Serial,
			})
			if err != nil {
				return out, err
			}
			out[id] = response
		}
	}
	return out, nil
}

func (cm *CertificateManager) ListAll(all bool, top int, opts ...ManagerOption) (out []*Certificate) {
	cm.applyOpts(opts...)
	var tmp []*Certificate
	for serial, entry := range cm.certMap {
		if !all {
			if time.Now().After(entry.certificate.NotAfter) {
				continue
			}
		}
		tmp = append(tmp, NewCertificate(cm, entry, serial))
	}
	if cm.SortFunc != nil {
		cm.SortFunc(tmp)
	}
	var it int
	for _, e := range tmp {
		if top > 0 && it >= top {
			return
		}
		out = append(out, e)
		it++
	}

	return
}

// Tidy - this endpoint allows tidying up the storage backend and/or CRL by removing certificates that have expired and are past a certain buffer period beyond their expiration time.
// 	@param safetyBuffer defaults to [72h]
// 	@param certStore defaults to [false]
// 	@param revokedCerts defaults to [false]
func (cm *CertificateManager) Tidy(safetyBuffer string, certStore, revokedCerts bool) (*vault.Secret, error) {
	return cm.logical.Write(fmt.Sprintf("%s/tidy", cm.pkiMountPoint), map[string]interface{}{
		"safety_buffer":      safetyBuffer,
		"tidy_cert_store":    certStore,
		"tidy_revoked_certs": revokedCerts,
	})
}

func (cm *CertificateManager) TidyStatus() (*vault.Secret, error) {
	return cm.logical.Read(fmt.Sprintf("%s/tidy-status", cm.pkiMountPoint))
}

func (cm *CertificateManager) applyOpts(opts ...ManagerOption) {
	for _, opt := range opts {
		opt(cm)
	}
}
