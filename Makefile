# Target configuration
TARGET_OS		:= linux
TARGET_ARCH		:= amd64
CGO_ENABLED		:= 0

VERSION			= $(shell git rev-list -1 HEAD)
APP_NAME		:= vault-pki
VERSION_SETTING	= gitlab.com/pcanilho/vault-pki/cmd.version

build:
	go get ./...
	GOOS=$(TARGET_OS) GOARCH=$(TARGET_ARCH) CGO_ENABLED=$(CGO_ENABLED) \
	go build -ldflags "-X $(VERSION_SETTING)=$(VERSION)" -o $(APP_NAME).$(TARGET_OS)
